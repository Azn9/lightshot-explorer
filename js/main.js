const characters = 'abcdefghijklmnopqrstuvwxyz0123456789';

function generate() {
    const s = [];

    for (let i = 0; i < 6; i++) s.push(getRandomChar());

    window.open("https://prnt.sc/" + s.join(''), '_blank');
}

function getRandomChar() {
    return characters.charAt(Math.floor(Math.random() * characters.length))
}